package no.uib.inf101.ghostlabyrinth;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.ghostlabyrinth.controller.MainController;
import no.uib.inf101.ghostlabyrinth.model.GhostLabModel;
import no.uib.inf101.ghostlabyrinth.view.MainView;

import javax.swing.JFrame;
import java.util.Random;

/**
 * The main class for the Ghost Labyrinth game.
 */
public class MainGhostLab {
  private static final String TITLE = "INF101 Ghost Labyrinth";

  public static void main(String[] args) {
    Random random = new Random();
    EventBus eventBus = new EventBus();
    GhostLabModel model = new GhostLabModel(eventBus, random);
    MainView view = new MainView(eventBus, model);
    new MainController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle(MainGhostLab.TITLE);
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
