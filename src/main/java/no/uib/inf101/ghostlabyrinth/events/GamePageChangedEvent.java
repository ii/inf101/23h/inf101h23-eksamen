package no.uib.inf101.ghostlabyrinth.events;

import no.uib.inf101.eventbus.Event;

/**
 * An event that is fired when a change occurs in a game page.
 */
public record GamePageChangedEvent() implements Event {
}
