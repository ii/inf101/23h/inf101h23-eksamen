package no.uib.inf101.ghostlabyrinth.model.occupants;

import no.uib.inf101.ghostlabyrinth.view.ViewableCellOccupant;
import no.uib.inf101.grid.CellPosition;

import java.awt.image.BufferedImage;

/**
 * A cell occupant is an object that can be placed in a cell in the grid. The
 * occupant has a position in the grid and a sprite that can be used to
 * visualize the occupant.
 */
public abstract class CellOccupant implements ViewableCellOccupant {
  private CellPosition position;

  /** Creates a new occupant at the given position. */
  public CellOccupant(CellPosition position) {
    this.position = position;
  }

  /** Moves this occupant to the given position. */
  public void moveTo(CellPosition position) {
    this.position = position;
  }

  @Override
  public CellPosition getPosition() {
    return this.position;
  }

  @Override
  public abstract BufferedImage getSprite();

}
