package no.uib.inf101.ghostlabyrinth.model.occupants;

import no.uib.inf101.graphics.Inf101Graphics;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Direction;

import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * The ghost is a cell occupant that has a ghost sprite. Moreover, the ghost
 * has a delay and a next move, which is used by the ghost controller to
 * determine when to move the ghost next and in which direction.
 */
public class Ghost extends CellOccupant {
  private static final BufferedImage IMG = Inf101Graphics.loadImageFromResources("/sprites/ghost_yellow.png");

  private final Random random;

  /**
   * Creates a new ghost at the given position.
   *
   * @param position The position of the ghost.
   * @param random A random number generator (used to determine the next move).
   */
  public Ghost(CellPosition position, Random random) {
    super(position);
    this.random = random;
  }

  public int getDelay() {
    return 250;
  }

  public Direction getNextMove() {
    return Direction.values()[random.nextInt(Direction.values().length)];
  }

  @Override
  public BufferedImage getSprite() {
    return Ghost.IMG;
  }
}
