package no.uib.inf101.ghostlabyrinth.model;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.ghostlabyrinth.events.GamePageChangedEvent;
import no.uib.inf101.ghostlabyrinth.model.levels.Level;
import no.uib.inf101.ghostlabyrinth.model.occupants.CellOccupant;
import no.uib.inf101.ghostlabyrinth.model.occupants.Door;
import no.uib.inf101.ghostlabyrinth.model.occupants.Ghost;
import no.uib.inf101.ghostlabyrinth.model.occupants.Player;
import no.uib.inf101.ghostlabyrinth.view.ViewableGamePage;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Direction;
import no.uib.inf101.grid.IReadOnlyGrid;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * The game page is the part of the model that represents a single play
 * (scene) in the game. It consists of a maze, a player, a door and a
 * number of ghosts. Each cell occupant has a position in the maze.
 * <p>
 * Whenever the game page changes, a {@link GamePageChangedEvent} will
 * be posted on the event bus.
 */
public class GamePage implements ViewableGamePage {

  // Communication
  private final EventBus eventBus;

  // State variables
  private final IReadOnlyGrid<MazeTile> maze;
  private final List<Ghost> ghosts = new ArrayList<>();
  private final Player player;
  private final Door door;
  private GamePageState pageState = GamePageState.ALIVE;

  /**
   * Creates a new game page.
   *
   * @param eventBus The event bus to use for posting events. Must not be null.
   * @param random  The random number generator to use. Must not be null.
   * @param level  Which level to load.
   */
  public GamePage(EventBus eventBus, Random random, int level) {
    Objects.requireNonNull(eventBus, "The event bus cannot be null");
    Objects.requireNonNull(random, "The random number generator cannot be null");

    this.eventBus = eventBus;
    Level levelData = Level.load(level);
    this.maze = levelData.maze();
    this.player = new Player(levelData.playerPosition());
    this.door = new Door(levelData.doorPosition());
    for (CellPosition ghostPosition : levelData.ghostPositions()) {
      this.ghosts.add(new Ghost(ghostPosition, random));
    }
  }

  // GETTERS

  @Override
  public Player getPlayer() {
    return this.player;
  }

  @Override
  public Door getDoor() {
    return this.door;
  }

  @Override
  public IReadOnlyGrid<MazeTile> getMaze() {
    return this.maze;
  }

  @Override
  public List<Ghost> getGhosts() {
    return this.ghosts;
  }

  @Override
  public GamePageState getPageState() {
    return this.pageState;
  }

  // METHODS TO MUTATE THE GAME

  /**
   * Moves the agent (cell occupant) in the given direction if possible.
   * If the move is not possible, nothing happens.
   *
   * @param agent The agent to move. Must not be null.
   * @param dir The direction to move in. Must not be null.
   */
  public void moveAgent(CellOccupant agent, Direction dir) {
    Objects.requireNonNull(agent, "The agent cannot be null");
    Objects.requireNonNull(dir, "The direction cannot be null");

    // Ignore any changes if the game is not in the playing state
    if (!Objects.equals(GamePageState.ALIVE, this.pageState)) {
      return;
    }

    CellPosition newPosition = agent.getPosition().movedTowards(dir);
    if (!isLegalPosition(newPosition)) {
      return;
    }
    agent.moveTo(newPosition);
    changeGameStateIfGameIsDone();
    this.eventBus.post(new GamePageChangedEvent());
  }

  private boolean isLegalPosition(CellPosition pos) {
    return this.maze.contains(pos) && Objects.equals(this.maze.get(pos), MazeTile.EMPTY);
  }

  private void changeGameStateIfGameIsDone() {
    if (playerHitAGhost()) {
      this.pageState = GamePageState.LOST;
    } else if (playerHitTheDoor()) {
      this.pageState = GamePageState.WON;
    }
  }

  private boolean playerHitTheDoor() {
    return Objects.equals(this.player.getPosition(), this.door.getPosition());
  }

  private boolean playerHitAGhost() {
    for (Ghost ghost : this.ghosts) {
      if (Objects.equals(this.player.getPosition(), ghost.getPosition())) {
        return true;
      }
    }
    return false;
  }
}
