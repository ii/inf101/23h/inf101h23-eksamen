package no.uib.inf101.ghostlabyrinth.controller;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Objects;

/**
 * A keypress controller that performs the specified action whenever a
 * key is pressed in the given view. The controller does not
 * distinguish between different keys.
 */
public class AnyKeyPressController extends KeyAdapter {

  private final Runnable action;

  /**
   * Creates a new AnyKeyPressController
   *
   * @param view The view in which to listen for key presses. Must not be null.
   * @param action The action to be performed when a key is pressed.
   */
  public AnyKeyPressController(Component view, Runnable action) {
    Objects.requireNonNull(view, "The view cannot be null");
    Objects.requireNonNull(action, "The action cannot be null");

    this.action = action;
    view.addKeyListener(this);
  }

  @Override
  public void keyPressed(KeyEvent e) {
    this.action.run();
  }
}
