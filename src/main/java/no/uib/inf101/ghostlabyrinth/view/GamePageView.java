package no.uib.inf101.ghostlabyrinth.view;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.ghostlabyrinth.events.GamePageChangedEvent;
import no.uib.inf101.ghostlabyrinth.model.MazeTile;
import no.uib.inf101.graphics.CellPositionToPixelConverter;
import no.uib.inf101.graphics.Inf101Graphics;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;
import no.uib.inf101.grid.IReadOnlyGrid;
import no.uib.inf101.observable.ObservableValue;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.util.Objects;

/**
 * A view that displays the game page. The game page consists of a maze,
 * a player, a door and a some number of ghosts.
 * <p>
 * This view is responsible for drawing the maze and the occupants in it.
 * It requires that {@link GamePageChangedEvent} is posted on the event bus
 * whenever the game page changes. This view will then repaint itself.
 */
public class GamePageView extends JPanel {

  private static final int MARGIN = 2;
  private final ObservableValue<? extends ViewableGamePage> model;
  private CellPositionToPixelConverter converter;

  /**
   * Creates a new game page view.
   *
   * @param eventBus The event bus to listen to for GamePageChangedEvent events. Must not be null.
   * @param model The game page to display. Must not be null.
   */
  public GamePageView(EventBus eventBus, ObservableValue<? extends ViewableGamePage> model) {
    Objects.requireNonNull(eventBus, "The event bus cannot be null");
    Objects.requireNonNull(model, "The model cannot be null");

    this.model = model;
    this.model.addValueChangedListener((src, new_page, old_page) -> this.repaint());

    this.setFocusable(true);
    this.requestFocusInWindow();
    this.setPreferredSize(new Dimension(500, 500));

    eventBus.register(e -> {
      if (e instanceof GamePageChangedEvent)
        this.repaint();
    });
  }


  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    ViewableGamePage gamePage = this.model.getValue();
    initCellPositionToPixelConverter(gamePage.getMaze());
    drawMaze(g2, gamePage.getMaze());
    drawCellOccupant(g2, gamePage.getDoor());
    drawCellOccupant(g2, gamePage.getPlayer());
    for (ViewableCellOccupant ghost : gamePage.getGhosts()) {
      drawCellOccupant(g2, ghost);
    }
  }

  private void initCellPositionToPixelConverter(GridDimension gridDimension) {
    this.converter = new CellPositionToPixelConverter(
        this.getBounds(), gridDimension, MARGIN
    );
  }

  private void drawMaze(Graphics2D g2, IReadOnlyGrid<MazeTile> maze) {
    for (GridCell<MazeTile> gc : maze) {
      Color color = Objects.equals(gc.value(), MazeTile.WALL) ? Color.DARK_GRAY : Color.LIGHT_GRAY;
      g2.setColor(color);
      Rectangle2D rect = this.converter.getBoundsForCell(gc.pos());
      g2.fill(rect);
    }
  }

  private void drawCellOccupant(Graphics2D g2, ViewableCellOccupant cellOccupant) {
    Rectangle2D rect = this.converter.getBoundsForCell(cellOccupant.getPosition());
    Image image = cellOccupant.getSprite();
    double scale = getScale(image, rect);
    Inf101Graphics.drawCenteredImage(g2, image, rect.getCenterX(), rect.getCenterY(), scale);
  }

  private double getScale(Image image, Rectangle2D rect) {
    // Find the largest scale factor such that image is contained completely within rectangle
    double scaleX = rect.getWidth() / image.getWidth(null);
    double scaleY = rect.getHeight() / image.getHeight(null);
    return Math.min(scaleX, scaleY);
  }
}
