package no.uib.inf101.grid;

/**
 * A direction in a grid.
 */
public enum Direction {
  NORTH(0, -1),
  EAST(1, 0),
  SOUTH(0, 1),
  WEST(-1, 0),
  NONE(0, 0);

  private final int dx;
  private final int dy;

  Direction(int dx, int dy) {
    this.dx = dx;
    this.dy = dy;
  }

  /** Gets the change in x-coordinate when moving in this direction. */
  public int dx() {
    return dx;
  }

  /** Gets the change in y-coordinate when moving in this direction. */
  public int dy() {
    return dy;
  }
}
