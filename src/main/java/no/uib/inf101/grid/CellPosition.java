package no.uib.inf101.grid;

/**
 * A position in a grid.
 * 
 * @param row the row
 * @param col the column
 */
public record CellPosition(int row, int col) {

  /**
   * Returns a new position that is moved one step in the given direction
   * from this position.
   *
   * @param dir the direction to move
   * @return a new position that is one step removed from this position
   */
  public CellPosition movedTowards(Direction dir) {
    return new CellPosition(row + dir.dy(), col + dir.dx());
  }
}
