package no.uib.inf101.grid;

import java.util.function.Function;

/**
 * A read-only grid. The implementing methods should be non-mutating.
 *
 * @param <E> the type of values in the grid
 */
public interface IReadOnlyGrid<E> extends GridDimension, Iterable<GridCell<E>> {

  /**
   * Get the value at the given position.
   * 
   * @param pos the position
   * @return the value at the given position
   */
  E get(CellPosition pos);


  /**
   * Get a pretty string representation of the grid displaying the elements
   * in row-major order, using the given row and column separators.
   * @param rowSep the row separator
   * @param colSep the column separator
   * @param toString a function that converts the element type to a string
   * @return a pretty string representation of the grid
   */
  default String toPrettyString(String rowSep, String colSep, Function<E, String> toString) {
    StringBuilder sb = new StringBuilder();
    for (int row = 0; row < this.rows(); row++) {
      for (int col = 0; col < this.cols(); col++) {
        E value = this.get(new CellPosition(row, col));
        sb.append(toString.apply(value));
        if (col < this.cols() - 1) {
          sb.append(colSep);
        }
      }
      if (row < this.rows() - 1) {
        sb.append(rowSep);
      }
    }
    return sb.toString();
  }
}
