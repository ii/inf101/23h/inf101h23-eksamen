package no.uib.inf101.ghostlabyrinth.model;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.ghostlabyrinth.model.occupants.Ghost;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Direction;
import no.uib.inf101.grid.IReadOnlyGrid;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class TestGhostLabModel {

  private GhostLabModel model;

  @BeforeEach
  void prepare() {
    Random random = new Random(42);
    EventBus eventBus = new EventBus();
    this.model = new GhostLabModel(eventBus, random);
  }

  @Test
  void initialState() {
    assertEquals(GameState.WELCOME, this.model.getGameState().getValue());
    this.model.startGame();

    IReadOnlyGrid<MazeTile> maze = this.model.getGamePage().getValue().getMaze();
    String prettyString = maze.toPrettyString("\n", "", tile -> switch (tile) {
      case WALL -> "#";
      case EMPTY -> "-";
    });
    assertEquals("""
        ---#------
        ---#------
        ---#------
        ----------
        ----------
        ----------
        ----------
        ----------
        ----------
        ----------""",
        prettyString
    );

    assertEquals(GameState.PLAYING, this.model.getGameState().getValue());
    assertEquals(0, this.model.getLevel().getValue());
    assertEquals(new CellPosition(0, 0), this.model.getGamePage().getValue().getPlayer().getPosition());
    assertEquals(1, this.model.getGamePage().getValue().getGhosts().size());
    List<Ghost> ghosts = this.model.getGamePage().getValue().getGhosts();
    assertEquals(new CellPosition(5, 5), ghosts.get(0).getPosition());
  }

  @Test
  void movePlayerTest() {
    this.model.startGame();
    GamePage firstGamePage = this.model.getGamePage().getValue();

    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.SOUTH);
    assertEquals(new CellPosition(1,0), firstGamePage.getPlayer().getPosition());

    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.EAST);
    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.EAST);
    assertEquals(new CellPosition(1,2), firstGamePage.getPlayer().getPosition());

    // Hit a wall, no movement
    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.EAST);
    assertEquals(new CellPosition(1,2), firstGamePage.getPlayer().getPosition());

    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.NORTH);
    assertEquals(new CellPosition(0,2), firstGamePage.getPlayer().getPosition());

    // Hit outside, no movement
    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.NORTH);
    assertEquals(new CellPosition(0,2), firstGamePage.getPlayer().getPosition());

    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.WEST);
    assertEquals(new CellPosition(0,1), firstGamePage.getPlayer().getPosition());
  }

  @Test
  void moveToWinTest() {
    this.model.startGame();
    GamePage firstGamePage = this.model.getGamePage().getValue();
    for (int i = 0; i < 9; i++) {
      firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.SOUTH);
    }
    for (int i = 0; i < 8; i++) {
      firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.EAST);
    }
    // Before move to win
    assertEquals(firstGamePage, this.model.getGamePage().getValue());
    assertEquals(GameState.PLAYING, this.model.getGameState().getValue());
    assertEquals(GamePageState.ALIVE, firstGamePage.getPageState());
    assertEquals(new CellPosition(9,8), firstGamePage.getPlayer().getPosition());

    // Move to win
    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.EAST);

    // After move to win
    assertEquals(GamePageState.WON, firstGamePage.getPageState());
    assertEquals(GameState.WON, this.model.getGameState().getValue());

    // Go to next level
    this.model.goToNextLevel();
    assertEquals(GameState.PLAYING, this.model.getGameState().getValue());
    assertNotEquals(firstGamePage, this.model.getGamePage().getValue());
  }

  @Test
  void moveToLoseTest() {
    this.model.startGame();
    GamePage firstGamePage = this.model.getGamePage().getValue();
    for (int i = 0; i < 5; i++) {
      firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.SOUTH);
    }
    for (int i = 0; i < 4; i++) {
      firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.EAST);
    }
    // Before move to lose
    assertEquals(GameState.PLAYING, this.model.getGameState().getValue());
    assertEquals(GamePageState.ALIVE, firstGamePage.getPageState());
    assertEquals(new CellPosition(5,4), firstGamePage.getPlayer().getPosition());

    // Move to hit ghost
    firstGamePage.moveAgent(firstGamePage.getPlayer(), Direction.EAST);

    // Check that we got the loss event
    assertEquals(GamePageState.LOST, firstGamePage.getPageState());
    assertEquals(GameState.GAME_OVER, this.model.getGameState().getValue());
  }
}
